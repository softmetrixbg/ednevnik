<?php

require ('initialize.php');
//parents
$db = Database::getInstance()->getConnection();
$sql = "SELECT * from students";
$st = $db->prepare($sql);
$st->execute();
$students = $st->fetchAll();

$sql = "SELECT * from users where status=4";
$st = $db->prepare($sql);
$st->execute();
$parents = $st->fetchAll();

for($i=0,$c=count($parents); $i<$c; $i++) {
    $parentID = $parents[$i]->users_id;
    $student_JMBG = $students[$i]->student_JMBG;
    $sql = "INSERT INTO parents VALUES ({$parentID},{$student_JMBG})";
    $db->query($sql);
    $sql = "UPDATE students set parents_parents_id = {$parentID} WHERE student_JMBG={$student_JMBG}";
    $db->query($sql);
}
//teachers
$sql = "SELECT * from users where status=3";
$st = $db->prepare($sql);
$st->execute();
$teachers = $st->fetchAll();

for($i=0, $c=count($teachers); $i<$c; $i++) {
    $sql = "INSERT INTO teachers VALUES ({$teachers[$i]->users_id})";
    $db->query($sql);
    $group = $i+1;
    $sql = "UPDATE student_group SET teachers_teachers_id={$teachers[$i]->users_id} WHERE student_group_id={$group}";
    $db->query($sql);
}
echo "done";