<?php

function checkStatus() {
  $status = substr($_COOKIE['session'],-1);
  $status = 2;
  if(!is_numeric($status) || $status < 1 || $status >4) {
    header('location:login.php');
  }
  $currentFile = getCurrentFileName();
  $currentDir = getCurrentDirectoryName();
  // $permissions[1] = ['users.php','groups.php','subjects.php','schedule.php','notifications.php']; //admin
  // $permissions[2] = ['statsgroups.php','statssubjects.php']; // direktor
  // $permissions[3] = ['gradebook.php','messages.php','opendoors.php','schedule.php','group.php','proba.php','teacher.php']; // ucitelj
  // $permissions[4] = ['messages.php','notifications.php','opendoors.php']; //roditelj
  $defaultpage = [  1=> 'admin.php',
                    2=>'director.php',
                    3=>'teacher.php',
                    4=>'parent.php'];
  $dirPerm [1] = 'admin';
  $dirPerm [2] = 'director';
  $dirPerm [3] = 'teacher';
  $dirPerm [4] = 'parent';
  // if(in_array($currentFile,$permissions[$status])) {
  //   return true;
  // } 
  if($dirPerm[$status]===$currentDir) {
    return true;
  }
  else {
    $redirect = 'location:http://localhost/_ednevnik/'.$dirPerm[$status].'/index.php';
    header($redirect);
  }

}

//uzima ukupan broj studenata iz baze
function numOfStudents(){
  Mapper::set_database();
  $count = Mapper::count_all('students');
  return $count;

}


function getCurrentFileName() {
  $backtrace = debug_backtrace();
  $backtrace = end($backtrace);
  $string = $backtrace['file'];
  $len = strlen($string) - strrpos($string,"\\")-1;
  $file = substr($string,-$len);
  return $file;
}





function getCurrentDirectoryName() {
  $dir = getcwd();
  $len = strlen($dir) - strrpos($dir,"\\")-1;
  $dir = substr($dir,-$len);
  return $dir;
}
function getCurrentBlock() {
  $hour = date('G')+2;
  $minutes = date('i');
  $time = $hour * 3600 + $minutes*60;
  $blocks = Mapper::selectAllItems('blocks');
  foreach($blocks as $block) {
      if($time > $block->blockstart && $time < $block->blockstart+45*60) {
          $currentBlock = $block->blocks_id;
          break;
      }
  }
  return $currentBlock;
}

function getCurrentDay() {
  $days=['Mon','Tue','Wed','Thu','Fri'];
  $day = date('D');
  $currentDay = array_search($day,$days) +1;
  return $currentDay;
}


function avatar($file, $size) {
  $k = pathinfo($file);
  $avatar = '../private/avatars/'.$k['filename'].'_'.$size.$k['dirname'].$k['extension'];
  if(file_exists($avatar)) {
      return $avatar;
  } else $avatar = '../private/avatars/default.jpg';
  return $avatar;
} // bice metoda za display, mozda na osnovu toga gde se nalazis(DIR_NAME) da se
// bira size, koji moze po difoltu da se stavi da je null

function uploadAvatar() {
  $max = 500 * 1024;
  $result = array();
  $targetDir = __DIR__ . '/avatars/';
  echo $targetDir;
  try {
      $upload = new FileUpload($targetDir);
      $upload->setMaxSize($max);
      //$upload->allowAllTypes();
      $upload->setIdAsFileName();
      // rename duplicates i setIdAsFileName se medjusobno kose
      $upload->upload();
      $upload->saveAvatarInDb();
      $result = $upload->getMessages();
  } catch (Exception $e) {
      $result[] = $e->getMessage();
  }

  //ovo ispod bi trebalo da je deo aploada, jer ako ne prodje apload, ne postoji slika koja bi trebala da se risajzuje
  if(isset($result['filename']) && !$upload->allTypesCalled) {
      $file = $targetDir . $result['filename'];
      $destination = $targetDir;
      try {
          $resize = new ImageResize($file, true);
          $resize->setOutputSizes(AVATAR_SIZES);
          $result = $resize->outputImage($destination);
          $resize->deleteSimilar();
      } catch (Exception $e) {
          echo $e->getMessage();
      }
  }
}

function uploadPoster($name = null) {
  $max = 5000 * 1024;
  $result = array();

  $targetDir = __DIR__ . '/posters/';
  try {

      $upload = new FileUpload($targetDir);
      $upload->setMaxSize($max);
      //$upload->allowAllTypes();
      //$upload->setIdAsFileName();
      if(isset($name)) {
          $upload->setPosterName($name);
      }
      $upload->upload();
      $upload->savePosterInDb($name);
      $result = $upload->getMessages();
      print_r($result);
  } catch (Exception $e) {
      $result[] = $e->getMessage();
  }
  if(isset($result['filename']) && !$upload->allTypesCalled) {
      $file = $targetDir . $result['filename'];
      $destination = $targetDir;
      try {
          $resize = new ImageResize($file, true);
          $resize->setOutputSizes(POSTER_SIZES);
          $result = $resize->outputImage($destination);
          $resize->deleteSimilar();
      } catch (Exception $e) {
          echo $e->getMessage();
      }
  }
}

function picSmall($img) {
  return "../private/posters/".substr_replace($img,'_300',strpos($img,'.'),0);
}

function picBig($img) {
  return "../private/posters/".substr_replace($img,'_400',strpos($img,'.'),0);
}

function h($string) {
  return htmlspecialchars($string);
}

function u($string) {
  return urlencode($string);
}

function urlFor($path) {
  if($path[0] != '/') {
      $path = "/" . $path;
  }
  return WWW_ROOT . $path;
}