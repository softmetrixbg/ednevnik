<?php

define("DB_HOST", "localhost");
define("DB_NAME", "e_dnevnik_");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_CHARSET", "utf8");
define("PRIVATE_PATH", dirname(__FILE__));
define("PROJECT_PATH", dirname(PRIVATE_PATH));
define("PUBLIC_PATH", PROJECT_PATH . '/public');
define("SHARED_PATH", PRIVATE_PATH . '/shared');
define("BLOCKTIMES", array(27000,30000,33600,36600,39600,42600));
const PICTURE_SIZES = array(200,500);
require_once('autoload.php');
require_once('functions.php');
if(strcmp('login.php',getCurrentFileName())!==0){
     //$session = new Session();
     //$session->startSession();
     //checkStatus();
}