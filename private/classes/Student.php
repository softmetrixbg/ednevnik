<?php

class Student {
    public $student_id;
    public $name;
    public $lastName;
    public $email;
    public $group_id;
    public $jmbg;

    public $parent_id;
    public $grades = array();
    public $tempGrades = array();

    public function __construct($args=[]) {
            $this->student_id   = isset($args['students_id']) 	? $args['students_id'] 	: '';
            $this->name         = isset($args['name']) 			? $args['name'] 		: '';
            $this->lastName     = isset($args['lastName']) 		? $args['lastName']    	: '';
            $this->email        = isset($args['email']) 		? $args['email']        : '';
            $this->group_id     = isset($args['group_id']) 		? $args['group_id']    	: '';
            $this->jmbg         = isset($args['students_JMBG']) ? $args['students_JMBG']: '';
            $this->parent_id    = isset($args['parent']) 		? $args['parent']    	: '';
			$this->tempGrades	= isset($args['subjects']) 		? $args['subjects']    	: '';
	}

    public function fillAllGrades() {
        $this->prepareSubjects();
        $this->prepareSemestar();
        foreach($this->grades as $subject) {
            foreach($subject->semestar as $sem)
            $sem->grades[] = Mapper::findGrades($this->student_id,$subject->subject_id,$sem->semestar_id);
        }
    }

    protected function prepareSubjects() {
        $subjects = Mapper::selectAllItems('subjects');
        foreach ($subjects as $subject) {
            $s = (array) $subject;
            $this->grades[] = new Subject($s);
        }
        //$this->grades = Mapper::selectAllItems('subjects');
    }

    protected function prepareSemestar() {
        foreach($this->grades as $subject) {
            $subject->semestar = Mapper::selectAllItems('semestar');
        }
    }


    public function setGrades() {
        foreach ($this->tempGrades as $subject) {
            $subject = new Subject($subject);
            $subject->prepareObject();
            $this->grades[]=$subject;
        }
		unset($this->tempGrades);
    }

	public function fillEmptyData() {
		
	}

}