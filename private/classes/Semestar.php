<?php

class Semestar {
    public $semestar_id;
    public $semestar_name;
    public $grades = array();

    public function __construct($args) {
        $this->semestar_id   = isset($args['semesterId'])   ? $args['semesterId']   : '';
        $this->semestar_name = isset($args['name']) 		? $args['name'] 		: '';
        $this->grades = $args['grades'];
    }
}