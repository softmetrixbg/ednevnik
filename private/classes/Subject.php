<?php

class Subject {
    public $subject_id;
    public $subject_name;

    public $semester = array();
    public $tempData = array();

    public function __construct($args=[]) {
        $this->subject_id   = isset($args['subjectId']) ? $args['subjectId']: '';
        $this->subject_name = isset($args['name'])		? $args['name']		: '';
        $this->tempData		= $args['semester'];
    }
	
	public function prepareObject() {
		foreach($this->tempData as $sem) {
			$s = new Semestar($sem);
			$this->semester[] = $s;
		}
		unset($this->tempData);
	}
	public function fillEmptyData() {
		
	}
}