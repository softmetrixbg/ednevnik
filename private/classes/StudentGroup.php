<?php

class StudentGroup {
    public $group_id;
    public $groupYear;
    public $groupNumber;
    public $teacher;

    public $students = array();

    public function __construct($args=[]) {
        $this->group_id     = isset($args['student_group_id'])      ? $args['student_group_id']         : '';
        $this->groupYear    = isset($args['group_year']) ? $args['group_year']    : '';
        $this->groupNumber  = isset($args['group_number']) ? $args['group_number']    : '';
        $this->teacher      = isset($args['teachers_teachers_id']) ? $args['teachers_teachers_id']    : '';
    }


    public function addStudents($student) {

    }

    public function fillStudents() {

        $data   = Mapper::getGradesByGroup($this->group_id);
        foreach($data as $row) {
            if(!isset($grades[$row->students_id_fk]['name'])) {
              $grades[$row->students_id_fk]['name']=$row->studentName;
              $grades[$row->students_id_fk]['lastName']=$row->studentLastName;
              $grades[$row->students_id_fk]['email']=$row->email;
              $grades[$row->students_id_fk]['group_id']=$row->group_id;
              $grades[$row->students_id_fk]['parent']=$row->parents_parents_id;
              $grades[$row->students_id_fk]['students_id']=$row->students_id_fk;
            }
            if(!isset($grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['name'])) {
              $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['name'] = $row->subjectName;
              $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['subjectId'] = $row->subjects_id_fk;
            }
            if(!isset($grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['semester'][$row->semestar]['name'])){
                $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['semester'][$row->semestar]['name'] = $row->semestarName;
                $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['semester'][$row->semestar]['semesterId'] = $row->semestar;
            }

            $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['semester'][$row->semestar]['grades'][$row->grade_type][]=$row->value;
        }
        $students = $grades;

        $grades[$row->students_id_fk]['subjects'][$row->subjects_id_fk]['semester'][$row->semestar]['grades'][$row->grade_type]=$row->value;
        
    

        $students = Mapper::getGradesByGroup($this->group_id);
        foreach($students as $student) {
            $s = new Student($student);
            $s->setGrades();
			$this->students[] = $s;
        }
    }

    public function fillStudentsGrades() {
        if(empty($this->students)) {
            echo "no students";
        } else {
            foreach($this->students as $student) {
                $student->fillAllGrades();
            }    
        }

    }
}