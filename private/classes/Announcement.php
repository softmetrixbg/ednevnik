<?php

class Announcement extends Mapper {
    public $announcement_id;
    public $subject;
    public $body;
    public $sender;
    public $target;
    public $timesent;

    public function __construct($args = []) {
        $this->announcement_id      = isset($args['announcement_id']) ? $args['announcement_id'] : '';
        $this->subject = isset($args['subject']) ? $args['subject'] : '';
        $this->body    = isset($args['body'])    ? $args['body']    : '';
        $this->timesent= isset($args['time'])    ? $args['time']    : '';
        $this->sender  = isset($args['sender'])  ? $args['sender']  : '';
        $this->target  = isset($args['target'])  ? $args['target']  : '';
        $this->firstName  = isset($args['firstName'])  ? $args['firstName']  : '';
        $this->lastName  = isset($args['lastName'])  ? $args['lastName']  : '';
        Mapper::set_database();
    }

    public function setTargets($args = []) {
        $this->sender = $_SESSION['id'];
        $this->timesent   = time();
        if(empty($args)) {
            $this->target = self::getParents($this->sender);
        } else {
            $this->target = $args;
        }

    }

    public function send() {
        // provera da li je sve ok
        //poziv metode iz mapera
        self::sendAnnouncement($this);
        //vracanje informacije
    }

    public function showNotification() {
    }
}