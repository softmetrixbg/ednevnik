<?php
require('../private/initialize.php');
Mapper::set_database();
// uzima sva odeljenja, dane i predmete
$groups	  = Mapper::getGSids();
$days     = Mapper::selectAllItems('days');
$subjects = Mapper::selectAllItems('subjects');
//proverava da li je zatrazen raspored nekog specificnog odeljenja, ako nije, stavlja schedule_id za prvo odeljenje
if(isset($_POST['schedule_id'])) {
    $schedule_id = $_POST['schedule_id'];
} else if (isset($_POST['student_group_id'])) {
    $schedule_id = Mapper::getScheduleId($_POST['student_group_id']);
} else $schedule_id = 1;
// proverava da li je menjan i sabmitovan novi raspored
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['updateschedule'])) {
    $schedule = json_decode($_POST['schedule']);
	//brise stari raspored u scheduleblocks
    Mapper::removeSchedule($schedule_id);
	//ubacuje novi raspored u bazu
    foreach($schedule as $day) {
        foreach($day->blocks as $id=>$block) {
            $block_id =(int) $id + 1;
            if($block ===0) 
                continue;
            $temp = explode("|",$block);
            $subject_id =(int) $temp[1];
            Mapper::insertBlock($schedule_id,$block_id,$subject_id,$day->id);
        }
    }
}

$schedule = Mapper::getSchedule($schedule_id);
$jssubjects[0] = "a";
foreach ($subjects as $s) {
    $jssubjects[] = $s->name;
}
$js = json_encode($jssubjects);
?>
<!DOCTYPE HTML>
<html>
<head>
    <style>
        body {
            font-size:25px;
        }
        table {
            width:100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
            text-align: left;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
        }
        #subjects {
        width: 120px;
        height: 235px;
        margin: 10px;
        padding: 10px;
        border: 1px solid black;
    }
    </style>
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
        function populate($id) {
            var subjects = <?=$js?>;
            var element = document.createElement("p");
            var r = Math.random().toString(36).substring(7);
            element.id = r+"|"+$id;
            element.draggable="true";
            element.ondragstart = drag;
            element.innerHTML = subjects[$id];
            document.getElementById('subjects').appendChild(element);
        }

        function getSchedule() {
            var days = [
                {
                    name:"njet",
                    id:0,
                    blocks:[]
                },
                {
                    name:"ponedeljak",
                    id: 1,
                    blocks: []
                },
                {
                    name:"utorak",
                    id: 2,
                    blocks: []
                },
                {
                    name:"sreda",
                    id: 3,
                    blocks: []
                },
                {
                    name:"cetvrtak",
                    id: 4,
                    blocks: []
                },
                {
                    name:"petak",
                    id: 5,
                    blocks: []
                }
            ];
            
            var table = document.getElementById("t01");
            for (var i = 0, row; row = table.rows[i]; i++) {
            //rows would be accessed using the "row" variable assigned in the for loop
                for (var j = 0, col; col = row.cells[j]; j++) {
                    child = col.firstElementChild;
                    if (child !== null) {
                        child = child.getAttribute("id");
                    } else {
                        child = 0;
                    }
                    days[i].blocks.push(child);
                    //columns would be accessed using the "col" variable assigned in the for loop
                }  
                days[i].blocks.shift();
            }
            days.shift();
            document.getElementById("raspored").value = JSON.stringify(days);
            console.log(days);
         }

    </script>
</head>
<body>
<?php 
foreach($groups as $g) {
    if($g->schedule_id==$schedule_id) {
        echo "<h1>Raspored za {$g->group_year}-{$g->group_number}</h1>";
        break;
    }
}
?>

<form action="schedule.php" method="POST">
    <select name="student_group_id">
  <?php 
  
  foreach($groups as $group) {
      echo "<option value={$group->student_group_id}>{$group->group_year}-{$group->group_number}</option>";
  }
  
  ?>
    </select>
  <input type="submit" name="getschedule" value="Get Schedule" >
</form>
<table id="t01">
    <tr>
    <th>Dan</th>
    <th>07:30 - 08:15</th>
    <th>08:20 - 09:05</th> 
    <th>09:20 - 10:05</th>
    <th>10:10 - 10:55</th>
    <th>11:00 - 11:45</th>
    <th>11:50 - 12:35</th>
    </tr>
    <?php
    if($schedule) {
    foreach ($schedule->days as $day) {
    ?>
    <tr>
        <td><?=$day->name?></td>
        <?php foreach ($day->blocks as $block) {
         $block->showBlock();   
        }?>
    </tr>
    <?php } }//END DAYS FOREACH ?>
</table>
Add Subject:
<select name="tip" id="slct1" onChange="populate(value)">
    <option value="">Izaberi</option>
    <?php 
    foreach($subjects as $subject) {
        echo "<option value='{$subject->subjects_id}'>{$subject->name}</option>";
    }
    ?>
</select>
<div id="subjects"  ondrop="drop(event)" ondragover="allowDrop(event)"></div>

<form action="schedule.php" method="POST" onsubmit="getSchedule()">
    <input type="hidden" name="schedule_id" value="<?=$schedule_id?>">
    <input type="hidden" id="raspored" name="schedule" value="">
    <input type="submit" name="updateschedule" value="UPDATE" >
</form>
</body>
</html>
