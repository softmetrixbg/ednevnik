<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php
    if(getCurrentFileName()==='opendoors.php') {
        ?>
        <link rel="stylesheet" href="/resources/demos/style.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

        <?php } else { ?>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <?php } ?>
    <title>SB Admin - Bootstrap Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../private/styles/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../private/styles/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../../private/styles/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        $(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
     
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <?php
    if(getCurrentFileName()==='opendoors.php') {
        ?>
        <style>
            .col-lg-4{
                position: absolute;
                right: 650px;

            }
            #text{
                background-color: white;
            }
            #wrapper{

                background-color: antiquewhite;
                margin-right: 200px;
                 }
            .error{
                color: red;
                background-color: white;
            }
            textarea{
                height: 600px;
            }
        </style>
        <?php

    }
    ?>
    <?php
    if(getCurrentFileName()==='teacher.php') {
        ?>
        <style>
            table {
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
            }
            th, td {
            text-align: left;
            border: 1px solid;
            padding: 8px;
            cursor: pointer;
            width: 100px;
            }
            th{background-color: #51D5FF;}
            tr:nth-child(even) {background-color: #f2f2f2;}
/*            #moving {
            display: none; 
            -webkit-transition: width 2s;
            transition: width 2s;
            }

            .content:hover #moving{
            display: inline;
            position: absolute;
            background-color: #51D5FF;
            width: auto;
            height: 150px;
            border-radius: 10px;
            }*/
        </style>
        <?php

    } 
    if(getCurrentFIleName()==='axis.php' || getCurrentFIleName()==='axis_odeljenja.php') {
        ?>
        <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 9pt;
            background:white;
            }


            html, body {
                margin: 0;
                height: 100%;
            }


            #chartdiv {
            width: 100%;
            height: 79%;
            }

            #chartdiv1 {
            width: 100%;
            height: 60%;
            }
            .bigtime{
                height: 20%;

            }
            h1{
                text-align: center;
                vertical-align: middle;
            }
            input {
                background-color: #fff;
                border-radius: 8px;
                -webkit-transition-duration: 0.4s;
                transition-duration: 0.4s;
                width: 300px;
                height: 50px;
                border: 2px solid #f44336;
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
            }
            input:hover {
            background-color: #f44336;
            color: white;

            }
            select{
                height: 50px;
                border-radius: 16px;
            outline: white;
            }
            a{
                color: white;
                width: 100%;
            display: block;
            
            }

            .back {
            position: absolute;
            right:    0;
            bottom:   0;
            display: inline-block;
            border-radius: 4px;
            background-color: #f44336;
            color: #FFFFFF;
            text-align: center;
            font-size: 28px;
            width: 200px;
            transition: all 0.5s;
            cursor: pointer;
            }

            .back span {
            cursor: pointer;
            display: inline-block;
            position: relative;
            transition: 0.5s;
            }

            .back span:after {
            content: '\00bb';
            position: absolute;
            opacity: 0;
            top: 0;
            right: -20px;
            transition: 0.5s;
            }

            .back:hover span {
            padding-right: 25px;
            }

            .back:hover span:after {
            opacity: 1;
            right: 0;
            }
            .row{
            padding-left: 790px;
            }
            a:-webkit-any-link {
                text-decoration: none;
            }
            option{
            outline: white;
            }
            .navbar{
                display: none;
            }
            /**/


        </style>
        <?php
            }
            if(getCurrentFileName() === 'pick_page.php') {
                ?>
                <style>
        .side-nav{
            display: none;
                }
        .text-center{
            width: 30%;
                }
        a {
            text-align: center;
            vertical-align: middle;
            line-height: 200px;
            height: 100%;
            width: 100%;
            background-color: transparent;
            display: block;
            font-size: 20px;
        }
        a:hover{
            text-decoration: none;
        }
        .right{
            margin-right: 200px
        }
        .form-group {
            height: 48%;
            margin-bottom: 5px;
            margin-top: 50px;
            width: 100%;
            float: right;
            transition-duration: 0.4s;
            background-color: #ffff4d;
            border-radius: 70px;
        }
        .form-group:hover {
            background-color: #eada00;
            color: white;
            text-decoration: none;
        }
        .text-center {
            width: 80%;
            height: 100%;
            padding-left: 250px;
        }
        .row {
            height: 500px;
            margin-left: 65px;
            margin-right: 300px;
        }
        </style>
        <?php
    }

    if(getCurrentFileName()==='opendoors.php') {
        ?>
        <script>
            $( function() {
                $( "#datepicker" ).datepicker({
                    minDate: 0,
            beforeShowDay: $.datepicker.noWeekends,
                });
            } );
        </script>
        <?php } ?>
</head>

<body>
<?php
if(getCurrentFIleName()!='axis.php' && getCurrentFIleName()!='axis_odeljenja.php') {
        ?>
<div id="wrapper">
<?php } ?>