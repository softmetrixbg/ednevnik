<?php
require('../../private/initialize.php');  
if (isset($_POST['submit'])) {
	$dtime = date('Y-m-d', strtotime($_POST['dtime']));
	$reason = $_POST['reason'];
	if (!$dtime) {
		echo $dtime;
	}
	else {
		Mapper::set_database();
		$result = Mapper::openDoorSend($dtime,$reason);
	
		if ($result) {
			echo "Poslali ste zahtev za : ".$dtime.".";
			$teacher = Mapper::openDoorInvitation();
			$notification = "You have new open doors request.";
			Mapper::addNotification($notification,$teacher);
		}
		else {
			echo "Nije moguce zakazati";
		}
	}
}
?>           
<div class="row">
	<div class="col-lg-4 text-center">
		<form id="validation_form" method="post" action="opendoors.php">
			<div class="form-group">
				<label style='color: white;'>Datum:</label>
				<input type="text" id="datepicker" name="dtime" class="form-control"><br>
			</div>
			<div class="form-group">
				<label style='color: white;'>Razlog zakazivanja:</label>
				<textarea for="reason" rows="10" cols="50" name="reason" class="form-control" required="" maxlength="500"></textarea><br><br><br><br>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="zakaži" class="btn btn-success">
			</div>
		</form> 
	</div>
</div>
        
<script>
  $(document).ready(function() {
  $('form[id="validation_form"]').validate({
    rules: {
      dtime: 'required',
      reason: 'required',
    },
    messages: {
      dtime: 'Izaberite datum',
      reason: 'Navedite razlog zakazivanja',
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

});
</script> 

</body>
</html>