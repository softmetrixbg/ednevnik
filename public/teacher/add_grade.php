<?php 
include("includes/header.php");
?>
<?php 
include ("includes/top_nav.php");
?>
<?php  
include ("includes/sidebar.php");
?>
<?php 
include '../private/initialize.php';
include '../private/classes/Mapper.php';
Mapper::set_database();
 ?>
<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Add Grade
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-plus"></i><a href="add_grade.php"> Add grade</a> 
                </li>
                <li>
                    <i class="fa fa-recycle"></i><a href="#"> Delete grade</a> 
                </li>
                <li>
                    <i class="fa fa-key"></i><a href="#"> Edit grade</a> 
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
<?php include 'grade_management/addGrade.php'; ?> 
</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
<?php
include("includes/footer.php");

 ?>