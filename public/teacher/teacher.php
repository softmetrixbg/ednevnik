<?php 
include("../../private/initialize.php");
Mapper::set_database();
$grade_type = Mapper::selectAllItems('grade_type');
$semestar_id = Mapper::selectAllItems('semestar');
$predmeti = Mapper::selectAllItems('subjects');
if($_SERVER['REQUEST_METHOD']==='POST') { 
    //$_POST['ocene'] izgleda ovako 1|19|2|2|2 
    // 1. vrednost je subject_id 
    // 2. vrednost je student_id 
    // 3. vrednost je grade_type 
    // 4. vrednost je semestar_id 
    // 5. vrednost je grade 
    if(!isset($_POST['tip_ocene'])) 
    { 
        $data = explode('|',$_POST['ocena']); 
        $args['subject_id'] = (int) filter_var($data[0], FILTER_SANITIZE_NUMBER_INT); 
        $args['student_id'] = (int) filter_var($data[1], FILTER_SANITIZE_NUMBER_INT); 
        $args['grade_type'] = (int) filter_var($data[2], FILTER_SANITIZE_NUMBER_INT);
        $args['semestar_id'] = (int) filter_var($data[3], FILTER_SANITIZE_NUMBER_INT); 
        $args['grade'] = (int) filter_var($data[4], FILTER_SANITIZE_NUMBER_INT); 
        if(isset($_POST['delete'])) { 
            Mapper::deleteGrade($args); 
        } else if(isset($_POST['update'])) { 
            $newGrade['subject_id'] = $args['subject_id']; 
            $newGrade['student_id'] = $args['student_id']; 
            $newGrade['grade_type'] = $_POST['gradetype']; 
            $newGrade['semestar_id']= $_POST['semestar']; 
            $newGrade['grade'] = $_POST['ocena']; 
            Mapper::updateGrade($args,$newGrade); 
        } 
    } else { 
        $subjectAndStudentId = explode("|",$_POST['subject&student_id']); 
        //print_r($subjectAndStudentId); 
        $subjectId = trim($subjectAndStudentId[0],'"'); 
        $studentId = trim($subjectAndStudentId[1],'"'); 
        $grade = $_POST['ocena']; 
        $gradeTypeString = $_POST['tip_ocene']; 
        $semestar = $_POST['semestar']; 
        switch ($gradeTypeString) { 
            case 'pismeni zadatak': 
                $gradeType = 1; 
                break; 
            case 'pismena vezba': 
                $gradeType = 2; 
                break; 
            case 'usmeni': 
                $gradeType = 3; 
                break; 
        }
        if (!empty($grade && $semestar && $gradeTypeString) && $grade<=5 && $semestar<=6) { 
            $db = Database::getInstance()->getConnection(); 
            $msql = "INSERT INTO grades(value,subjects_id_fk,students_id_fk,grade_type,semestar) VALUES ($grade,$subjectId,$studentId,$gradeType,$semestar)"; 
            $gr = $db->prepare($msql); $gr->execute(); 
            $notification = "Your child has new grade";
            $parent =  (int) filter_var(end($subjectAndStudentId), FILTER_SANITIZE_NUMBER_INT); ;
            Mapper::addNotification($notification,$parent);
        } else{
            echo "error"; //dorada
        }
    } 

        if (!empty($grade && $semestar && $gradeTypeString)) { 
            $db = Database::getInstance()->getConnection(); 
            $msql = "INSERT INTO grades(value,subjects_id_fk,students_id_fk,grade_type,semestar) VALUES ($grade,$subjectId,$studentId,$gradeType,$semestar)"; 
            $gr = $db->prepare($msql); $gr->execute(); 
            $notification = "Your child has new grade";
            $parent =  (int) filter_var(end($subjectAndStudentId), FILTER_SANITIZE_NUMBER_INT); ;
            Mapper::addNotification($notification,$parent);
        } 
    
}  


$row = Mapper::SGall();
$student_group = new StudentGroup($row);
$student_group->fillStudents();

// print_r($student_group);
?>

<div id="page-wrapper">

<div class="container-fluid">
<?php if(isset($_GET['set']) && $_GET['set']==='add') {
    include '../../private/styles/css/addGradeForm.html';
    
}else {
    ?>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Izmenite ocenu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body mx-3">
                <form method="post" action="teacher.php">

                    <div class="md-form mb-5">
                        <?php 
                            $gradetypes = Mapper::selectAllItems('grade_type');
                            echo "<select name='gradetype' id='gradetype'>";
                            foreach($gradetypes as $gradetype) {
                                ?>
                                    <option value="<?=$gradetype->grade_type_id?>"><?=$gradetype->name?></option>
                                <?php
                            }
                        ?>
                        </select><br><br>
                    </div>
                    <input type="hidden" id="ocene" name="ocene" value="">
                    <div class="md-form mb-4">
                    <?php
                      
                      $semestar = Mapper::selectAllItems('semestar');
                      echo "<select name='semestar' id='semestar'>";
                      
                      foreach($semestar as $s) {
                          ?>
                              <option value="<?=$s->semestar_id?>"><?=$s->name?></option>
                          <?php
                      }

                    ?>       
                    </select> <br><br>
                    <input type="text" name="ocena" id="ocena" value="0">
                    </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button style="float:left" class="btn btn-warning" name="update">Update grade</button>
                        <button class="btn btn-danger" name="delete">Delete grade</button>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                    </div>
                </form>    
        </div>
    </div>
</div>
    <?php
}
?>
    


    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                GradeBook
                <small><?php Mapper::getStudentGroup();?></small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-plus"></i><a href="teacher.php?set=add"> Add grade</a> 
                </li>
                
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div style="overflow-x:auto;">
        <table style="width:100%">
            <tr>
                <th>Student</th>
                <?php 
                for ($i=0; $i < count(Mapper::selectAllSubjectsName()); $i++){
                    echo '<th>';
                    echo Mapper::selectAllSubjectsName()[$i];
                    echo "</th>";
                }
                ?>
            </tr>
            <?php
            foreach($student_group->students as $student) {
                echo "<tr>";
                echo "<td>".$student->name." ".$student->lastName."</td>";
                foreach($student->grades as $subject) {
                    echo "<td>";
                    foreach ($subject->semester as $semestar) {
                        foreach($semestar->grades as $grade) {

                            echo "<div  data-toggle='tooltip' data-placement='left' title=' ocena ".$grade->grade.", ".$subject->subject_name.",  ".$student->name." ".$student->lastName." , ".$semestar->semestar_name.", ".$grade->grade_type_name."'Tooltip on left' id='moving'><div class='content' id=".$subject->subject_id."|".$student->student_id."|".$grade->grade_type_id."|".$semestar->semestar_id." style='float:left; '>".$grade->grade."&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;
                            
                            </div></div>"; // on click za change/delete

                            // echo "<div data-toggle='modal' data-target='#modalLoginForm' id= ".$subject->subject_id."|".$student->student_id."|".$grade_type."|".$semestar->semestar_id."|".$grade." style='float:left;' onclick='myFunction(this.id,this.innerHTML)'>".$grade."</div>"; // on click za change/delete
                            ?>
                            <?php

                        }
                    }
                
                    if(isset($_GET['set']) && $_GET['set']==='add') {
                        echo "<input data-toggle='modal' data-target='#modalLoginForm' name='ocene' id= ".$subject->subject_id."|".$student->student_id."|".$student->parent_id." style='float:right' type='submit' value='+' onclick='myFunction(this.id)' >";
                        //echo "<button id= ".$subject->subject_id."|".$student->student_id." style='float:right' type='button' onclick='insertGrade(this.id)'>+</button>"; // onclick za unos ocene    
                    }
                    echo "</td>";
                }
                echo "</tr>";
            } ?>
        </table>
    </div>
    
    <form action="teacher.php" method="post">
        <input type="hidden" id="ocene" name="ocene" value="0" />
        <input type="submit" value="potvrdi" onclick='getchanges()' >

  
<script type="text/javascript">
 

  function myFunction(id,value) {
    document.getElementById('ocene').value = JSON.stringify(id);
    document.getElementById('ocena').value = value;
    var k = id.split("|");
    // 3. vrednost je grade_type
    gradeType = k[2];
    document.getElementById('gradetype').value=gradeType;
    // 4. vrednost je semestar_id
    semestar_id = k[3];
    document.getElementById('semestar').value=semestar_id;
  }

</script>

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

<?php
include("../../private/styles/includes/footer.php"); 