
<?php
//require('../../private/initialize.php');

if (isset($_POST['submit'])) {
	$dtime = date('Y-m-d', strtotime($_POST['dtime']));
	$reason = $_POST['reason'];
	if (!$dtime) {
		echo $dtime;
	}
	else {
		Mapper::set_database();
		$result = Mapper::openDoorSend($dtime,$reason);
	
		if ($result) {
			echo "Zakazali ste za : ".$dtime.".";
		}
		else {
			echo "Nije moguce zakazati";
		}
	}
}
?>           
<div class="row">
	<div class="col-lg-4 text-center">
		<form id="validation_form" method="post" action="opendoors.php">
			<div class="form-group">
				<label style='color: white;'>Datum:</label>
				<input type="text" id="datepicker" name="dtime" class="form-control"><br>
			</div>
			<div class="form-group">
				<label style='color: white;'>Razlog zakazivanja:</label>
				<textarea for="reason" rows="4" cols="50" name="reason" class="form-control" required=""></textarea><br><br><br><br>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="zakaži" class="btn btn-success">
			</div>
		</form> 
	</div>
</div>
        
<script>
  $(document).ready(function() {
  $('form[id="validation_form"]').validate({
    rules: {
      dtime: 'required',
      reason: 'required',
    },
    messages: {
      dtime: 'Izaberite datum',
      reason: 'Navedite razlog zakazivanja',
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

});
</script> 

</body>
</html>
<?php 
Mapper::set_database();

if(isset($_GET['accept'])) {
    Mapper::setRequest($_GET['accept'],1);
}
if(isset($_GET['reject'])) {
    Mapper::setRequest($_GET['reject'],0);
}

$requests = Mapper::getRequests();
?>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Open Doors
                </h1>
            </div>
        </div>

    <?php
    if(!empty($requests)) {
    foreach($requests as $req) {
        ?>
        <div class='row '>
            <div class='col-lg-8 '>
                <h2>You have new open doors request from <?=$req->firstName?>  <?=$req->lastName?></h2>
                <p><?=$req->message?></p>
                <p>Date: <?=$req->requesttime?></p>
                <a class='btn btn-success mt-2 ' style='margin-right:5px' href='?accept=<?=$req->opendoors_id?>' role='button'>Accept</a>
                <a class='btn btn-danger' href='?reject=<?=$req->opendoors_id?>' role='button'>Reject</a>
                </div>
        </div>
        <?php
    } } else {
        echo "<h2>No new requests</h2>";
    }
    ?>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

<?php
include("../../private/styles/includes/footer.php"); 
