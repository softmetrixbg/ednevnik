<?php
ob_start();
include ("../../private/initialize.php");
Mapper::set_database();
// uzima sva odeljenja, dane i predmete
$groups	  = Mapper::getGSids();
$days     = Mapper::selectAllItems('days');
$subjects = Mapper::selectAllItems('subjects');
//proverava da li je zatrazen raspored nekog specificnog odeljenja, ako nije, stavlja schedule_id za prvo odeljenje
$schedule_id = Mapper::getScheduleByTeacher();
$schedule = Mapper::getSchedule($schedule_id);
$jssubjects[0] = "a";
foreach ($subjects as $s) {
    $jssubjects[] = $s->name;
}

$js = json_encode($jssubjects);
?>
    <head>
        <style>
            table {
                font-size:16px;
            }
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            table#t01 th {
                background-color: #333;
                color: white;
            }
            #subjects {
                width: 100%;
                height: 235px;
                border-radius:8px;
                margin-top: 2px;
                border: 1px solid black;
            }
        </style>
        <script>
            function populate($id) {
                var subjects = <?=$js?>;
                var element = document.createElement("p");
                var r = Math.random().toString(36).substring(7);
                element.id = r+"|"+$id;
                element.innerHTML = subjects[$id];
                document.getElementById('subjects').appendChild(element);
            }

            function getSchedule() {
                var days = [
                    {
                        name:"njet",
                        id:0,
                        blocks:[]
                    },
                    {
                        name:"ponedeljak",
                        id: 1,
                        blocks: []
                    },
                    {
                        name:"utorak",
                        id: 2,
                        blocks: []
                    },
                    {
                        name:"sreda",
                        id: 3,
                        blocks: []
                    },
                    {
                        name:"cetvrtak",
                        id: 4,
                        blocks: []
                    },
                    {
                        name:"petak",
                        id: 5,
                        blocks: []
                    }
                ];

                var table = document.getElementById("t01");
                for (var i = 0, row; row = table.rows[i]; i++) {
                    for (var j = 0, col; col = row.cells[j]; j++) {
                        child = col.firstElementChild;
                        if (child !== null) {
                            child = child.getAttribute("id");
                        } else {
                            child = 0;
                        }
                        days[i].blocks.push(child);
                    }
                    days[i].blocks.shift();
                }
                days.shift();
                document.getElementById("raspored").value = JSON.stringify(days);
            }

        </script>
    </head>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    foreach($groups as $g) {
                        if($g->schedule_id==$schedule_id) {
                            echo "<h1 class='page-header'>Raspored za {$g->group_year}-{$g->group_number}</h1>";
                            break;
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.php"> Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-file"></i> Schedule
                        </li>
                    </ol>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <table id="t01">
                        <tr>
                            <th>Dan</th>
                            <th>07:30 - 08:15</th>
                            <th>08:20 - 09:05</th>
                            <th>09:20 - 10:05</th>
                            <th>10:10 - 10:55</th>
                            <th>11:00 - 11:45</th>
                            <th>11:50 - 12:35</th>
                        </tr>
                        <?php
                        if($schedule) {
                            foreach ($schedule->days as $day) {
                                ?>
                                <tr>
                                    <td><?=$day->name?></td>
                                    <?php foreach ($day->blocks as $block) {
                                        $block->showBlock();
                                    }?>
                                </tr>
                            <?php } }//END DAYS FOREACH ?>
                    </table>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->


<?php include("../../private/styles/includes/footer.php"); ?>