<script type="text/javascript" src="main.js"></script>
<?php 
require('../../private/initialize.php');
if ($_SERVER['REQUEST_METHOD']==="POST"){
    Mapper::updateStudentGroup();
}
?>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> Manage Groups
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> Blank Page
                    </li>
                </ol>
            </div>
        </div>
                <!--prikaz odeljenja u tabeli-->
        <div class="row">
            <div class="col-lg-8">
                <table class="table table-hover">
                    <tr>
                        <th>Group Id</th>
                        <th>Group Name</th>
                    </tr>
                        <?php
                            Mapper::set_database();
                            $row = Mapper::find_all("student_group");
                            foreach ($row as $group){
                        ?>
                    <tr>
                        <td><?php  echo $group->student_group_id; ?></td>
                        <td><?php  echo $group->group_year."-".$group->group_number; ?></td>
                        <td></td>
                        <td><a href= "edit_group.php?edit=<?php echo $group->student_group_id; ?>">Edit</a></td>
                    </tr>
                    <?php }    ?>
                </table>
                    <!-- editovanje oznacenih odeljenja -->
                    <?php
                        if(isset($_GET['edit'])){
                        $groupRow = Mapper::selectStudentGroupID();
                         foreach ($groupRow as $group){
                    ?>
                <form action="edit_group.php" method="POST">
                    <div class="form-group">
                            <label for="edit_name_group">Edit group <?php echo $group->group_year."-".$group->group_number; ?>:</label><br>
                            <input type="hidden" name="group_id" value="<?php echo $group->student_group_id;?>" class="form-control"><br>
                            <label for="group_year">Group year:</label>
                            <input type="number" name="group_year" value="" class="form-control"><br>
                            <label for="group_number">Group Number:</label>
                            <input type="number" name="group_number" value="" class="form-control"><br>
                            <input type="submit" class="btn btn-warning" name="edit_group" value="Edit">
                    </div>
                </form>
                             <?php } } ?>

             </div>
        </div>
    </div>
            <!-- /.container-fluid -->

</div>
            <!-- /#page-wrapper -->
        

  <?php include("../../private/styles/includes/footer.php"); ?>