<?php

include('../../private/initialize.php');
if($_SERVER['REQUEST_METHOD']==='POST') {
    $max = 1000 * 1024;
    $result = array();
    $targetDir  = __DIR__ . '/avatars/';
    $tempDir    = __DIR__ . '/temp/';
    if($_FILES['fileToUpload']['error']) {
        $content  = file_get_contents($_POST['link']);
        $fileType = substr($_POST['link'],strrpos($_POST['link'],'.'));
        $hash     = md5(random_bytes(15));
        $tempFile = $tempDir.$hash.$fileType;
        $fp = fopen($tempFile, "w");
        fwrite($fp, $content);
        fclose($fp);
        
    } else {
        try {
            $upload = new FileUpload($targetDir);
            $upload->setMaxSize($max);
            $upload->setIdAsFileName();
            $upload->upload();
            $upload->saveAvatarInDb();
            $result = $upload->getMessages();
        
        } catch(Exception $e) {
            $result[] = $e->getMessage();
        }

    }
    if(isset($result['filename']) && !$upload->allTypesCalled) {
        $file = $targetDir.$result['filename'];
        $destination = $targetDir;
        try {
            $resize = new ImageResize($file, true);
            $resize->setOutputSizes(PICTURE_SIZES);
            $result = $resize->outputImage($destination);
            $resize->deleteSimilar();
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

?>

<form action="proba.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="text" name="link" id="link">
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>