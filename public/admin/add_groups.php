<?php 
ob_start();
include ("../../private/initialize.php");
Mapper::set_database();
if(isset($_POST['add_group'])){
    Mapper::addStudentGroup();
    //dodati i u schedule tabelu novi raspored
}
$teachers = Mapper::getAvailableTeachers();
?>
<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Add group
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="index.php"> Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-file"></i>Add group
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <form action="add_groups.php" method="POST">
                    <div class="form-group">
                        <label for="group_year">Year:</label>
                        <input type="number" name="group_year" id="group_year" class="form-control"><br>
                        <label for="group_number">Class:</label>
                        <input type="number" name="group_number" id="group_number" class="form-control"><br>

                        <?php if(empty($teachers)) {
                            echo 'No available teachers.';
                        } else {?>
                        <select name='teacher'>
                            <?php
                            echo "<option value='0'>Select teacher</option>";
                            foreach($teachers as $teacher) {
                                echo "<option value='{$teacher->users_id}'>{$teacher->username}</option>";
                            }
                            ?>
                        </select>
                        <?php } ?><br>
                        <input type="submit" class="btn btn-primary" name="add_group" value="Add">
                    </div>
            </form>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
<script type="text/javascript" src="main.js"></script>
    
<?php include("../../private/styles/includes/footer.php"); ?>