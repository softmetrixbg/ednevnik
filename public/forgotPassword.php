<?php 
include('../private/initialize.php');
if($_SERVER['REQUEST_METHOD']==='POST'){
	if (isset($_POST['submit']) && !empty($_POST['email']) && !empty($_POST['username'])) {
		$email = $_POST['email'];
		$user = $_POST['username'];
		$pass = Mapper::getUserPassword($user);

		mail($email, "E-gradebook login password", "{$user} your password is {$pass}");
		//potrebno je enkriptovati password
		header("Location: http://localhost/_egradebook/public/login.php");
	}
	else{
		echo "<script>alert('Unesi username i email')</script>";
	}
}

 ?>
 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../private/styles/css/forgotPasswordStyle.css">

 <form action="forgotPassword.php" method="post">
  <div  class="form-group">
  	<label>Username</label>
    <input name="username" type="text" class="form-control" placeholder="Enter username">
    <br>
    <label for="exampleInputEmail1">Email</label>
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <br>
    <button name="submit" value="Send" type="submit" class="btn btn-primary">Send</button>
    <button id="rightButton" class="btn btn-link"><a href="login.php">back to login</a></button>
  </div>
  
  
</form>
