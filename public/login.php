<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <title>Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="../private/styles/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../private/styles/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../private/styles/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        label{color:#fff;}
    </style>
</head>

<body>

<div id="wrapper">

<?php
include("../private/initialize.php");
if($_SERVER['REQUEST_METHOD']==='POST' && isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    //connection on database
	$row = Mapper::getUsernameAndPassword($username,$password);
	if ($row == false) {
		echo "No exists in database";//dorada za gresku
	} else {
        $args['id'] = $row->users_id;
        $args['userStatus'] = $row->status_id;
        $args['username']   = $row->username;
        $session = new Session($args);
        $session->startSession();

		switch ($_SESSION['status']) {
			case 1:
				header('Location: http://localhost/_egradebook/public/admin/index.php');
				break;
			case 2:
				header('Location: http://localhost/_egradebook/public/director/index.php');
				break;
			case 3:
				header('Location: http://localhost/_egradebook/public/teacher/teacher.php');
                break;
			case 4:
				header('Location: http://localhost/_egradebook/public/parent/index.php');
					break;			
			default:
				header('Location: http://localhost/_egradebook/login.php');
				break;
		}
    }
}


?>

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-4">
            <form method="POST" action="login.php">
            <div class="form-group">
                <label>Name: </label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
                <label>Password: </label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
                <input type="submit" name="submit" value="Login" class="btn btn-primary">
                <a href="forgotPassword.php">forgot password?</a>
            </form>
            </div>