<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i>Users <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="">Add user</a>
                            </li>
                            <li>
                                <a href="#">Edit user</a>
                            </li>
                            <li>
                                <a href="#">Delete user</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-arrows-v"></i>Subjects <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                            <li>
                                <a href="add_subject.php">Add subjects</a>
                            </li>
                            <li>
                                <a href="edit_subject.php">Edit subjects</a>
                            </li>
                            <li>
                                <a href="delete_subject.php">Delete subjects</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>Class <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="">Add class</a>
                            </li>
                            <li>
                                <a href="#">Edit class</a>
                            </li>
                            <li>
                                <a href="#">Delete class</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-fw fa-table"></i>Manage user role</a>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>Group <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                            <li>
                                <a href="add_groups.php">Add group</a>
                            </li>
                            <li>
                                <a href="edit_group.php">Edit group</a>
                            </li>
                            <li>
                                <a href="delete_group.php">Delete group</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="forms.html"><i class="fa fa-fw fa-edit"></i>Manage parents</a>
                    </li>
                     <li>
                        <a href="opendoorssend.php"><i class="fa fa-fw fa-edit"></i>Obratite se nastavniku</a>
                    </li>
                </ul>
            </div>
            </nav>